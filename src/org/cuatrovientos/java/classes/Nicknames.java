package org.cuatrovientos.java.classes;

import java.util.Random;

public class Nicknames {
	/*
	 * Ejercicio 8:
	 */

	private String[] nicknameStart = { "Ojos", "Puño", "Espada", "Viento" };
	private String[] nicknameEnd = { "Negro", "de Fuego", "del Infierno", "Helada" };

	public Nicknames() {

	}

	public String generate() {
		Random rnd = new Random();
		return (nicknameStart[rnd.nextInt(4)] + " " + nicknameEnd[rnd.nextInt(4)]);
	}
}
