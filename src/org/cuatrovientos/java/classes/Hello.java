package org.cuatrovientos.java.classes;

public class Hello {

	/*
	 * Ejercicio 1: Crea una clase llamada Hello que simplemente
	 * tenga una propiedad String greet y un metodo sayHello() que muestre esa
	 * propiedad por la consola. Incluye el metodo main y crea una instancia de la
	 * clase para probarla. Crea el diagrama de clases UML
	 */

	private String greet;

	public Hello(String greet) {
		this.greet = greet;
	}

	public void sayHello() {
		System.out.println(greet);
	}
}
