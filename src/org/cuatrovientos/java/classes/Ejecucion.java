package org.cuatrovientos.java.classes;

import java.util.Scanner;

public class Ejecucion {
	private static Scanner sc;

	public static void main(String[] args) {
		// Ejercicio 1
		System.out.println("Ejercicio 1:");
		Hello hello = new Hello("Hola soy Cirigumin el mejor");
		hello.sayHello();
		// Ejercicio 2
		System.out.println("Ejercicio 2:");
		Coin c1 = new Coin();
		Coin c2 = new Coin();
		Coin c3 = new Coin();
		Coin c4 = new Coin();
		Coin c5 = new Coin();
		c1.flip();
		c2.flip();
		c3.flip();
		c4.flip();
		c5.flip();
		// Ejercicio 3
		System.out.println("Ejercicio 3:");
		Conversor c = new Conversor();
		sc = new Scanner(System.in);
		System.out.println("Introduzca la cantidad para convertir:");
		double x = Double.parseDouble(sc.nextLine());
		System.out.println(x + " Pesetas son " + c.pesetas2Euros(x) + " Euros");
		System.out.println(x + " Euros son " + c.euros2Pesetas(x) + " Pesetas");
		System.out.println(x + " Euros son " + c.euros2PDollars(x) + " Dolares");
		System.out.println(x + " Dolares son " + c.dollars2Euros(x) + " Euros");
		System.out.println(x + " Euros son " + c.euros2Pounds(x) + " Libras");
		System.out.println(x + " Libras son " + c.pounds2Euros(x) + " Euros");
		// Ejercicio 4
		System.out.println("Ejercicio 4:");
		Array array = new Array();
		System.out.println("Total even numbers: " + array.countEvent());
		array.increment();
		System.out.println("Total even numbers after increment: " + array.countEvent());
		array.decrement();
		System.out.println("Total even numbers after decrement: " + array.countEvent());
		// Ejercicio 5
		System.out.println("Ejercicio 5:");
		Dice d1 = new Dice();
		Dice d2 = new Dice(10);
		Dice d3 = new Dice(20, true);
		String sd1 = "";
		String sd2 = "";
		String sd3 = "";
		for (int i = 0; i < 100; i++) {
			sd1 = sd1 + " " + d1.roll();
			sd2 = sd2 + " " + d2.roll();
			sd3 = sd3 + " " + d3.roll();

		}
		System.out.println("Let's rool the dice:");
		System.out.println(sd1);
		System.out.println("Let's rool the D10:");
		System.out.println(sd2);
		System.out.println("Let's rool the D20, allowing 0:");
		System.out.println(sd3);
		// Ejercicio 6
		System.out.println("Ejercicio 6:");
		Square s1 = new Square();
		Square s2 = new Square('+');
		Square s3 = new Square('*');
		s1.show(3);
		s2.show(4);
		s3.show(8);
		// Ejercicio 7
		System.out.println("Ejercicio 7:");
		Names name1 = new Names();
		Names name2 = new Names(10);
		Names name3 = new Names(16);
		System.out.println(name1.generate());
		System.out.println(name2.generate());
		System.out.println(name3.generate());
		System.out.println(name1.generate());
		System.out.println(name2.generate());
		System.out.println(name3.generate());
		// Ejercicio 8
		System.out.println("Ejercicio 8:");
		Nicknames nicknames = new Nicknames();
		Names names = new Names();
		for (int i = 0; i < 5; i++) {
			System.out.println(names.generate() + " " + nicknames.generate());
		}
		// Ejercicio 9
		System.out.println("Ejercicio 9:");
		Passwords pass = new Passwords();
		Passwords pass1 = new Passwords(9);
		System.out.println(pass.generate());
		System.out.println(pass1.generate(4));
		// Ejercicio 9
		System.out.println("Ejercicio 10:");
		Javamon j1=new Javamon();
		Javamon j2=new Javamon();
		System.out.println(j1.status());	
		System.out.println(j2.status());	
		Combat combat=new Combat(j1, j2);
		System.out.println("Total "+combat.getAssaults()+" assaults");
		System.out.println("And the winner is:  "+combat.outcome().getName());
		System.out.println("Thanks for playing javamon");	
	}

}
