package org.cuatrovientos.java.classes;

public class Combat {

	/*
	 * Ejercicio 10:
	 */

	private int assaults;
	private Javamon javamon1;
	private Javamon javamon2;

	public Combat(Javamon javamon1, Javamon javamon2) {
		this.javamon1 = javamon1;
		this.javamon2 = javamon2;
		initCombat();
	}

	private void initCombat() {
		String asalto = "";
		while (this.javamon1.getLife() > 0 && this.javamon2.getLife() > 0) {
			// En caso de empate empieza javamon1
			int daño = 0;
			this.assaults = this.assaults + 1;
			if (this.javamon1.initiative() >= this.javamon2.initiative()) {
				asalto = asalto + javamon1.status() + " -> attacks -> " + javamon2.status() + "\n";
				daño =  assault(javamon1, javamon2);
				if (daño > 0) {
					asalto = asalto + javamon1.getName() + " -> makes " + daño + " damage to -> " + javamon2.getName()
							+ "\n";
				} else {
					asalto = asalto + " Asalto nulo daño:  " + daño + "\n";
				}
			} else {
				asalto = asalto + javamon2.status() + " -> attacks -> " + javamon1.status() + "\n";
				daño = assault(javamon2, javamon1);
				if (daño > 0) {
					asalto = asalto + javamon2.getName() + " -> makes " + daño + " damage to -> " + javamon1.getName()
							+ "\n";
				} else {
					asalto = asalto + " Asalto nulo daño:  " + daño + "\n";
				}
			}
		}
		System.out.println(asalto);
	}

	private int assault(Javamon javamon1, Javamon javamon2) {
		int daño = javamon1.attack() - javamon2.defend();
		if (daño > 0) {
			javamon2.setLife(javamon2.getLife() - daño);
		}
		return daño;
	}

	public Javamon outcome() {
		if (this.javamon1.getLife() <= 0) {
			return this.javamon2;
		} else {
			return this.javamon1;
		}

	}

	public int getAssaults() {
		return this.assaults;
	}
}
