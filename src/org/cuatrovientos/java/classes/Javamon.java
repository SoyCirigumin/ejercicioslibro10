package org.cuatrovientos.java.classes;

import java.util.Random;

public class Javamon {
	/*
	 * Ejercicio 10:Ponemos la condicion de q cada atributo agility, defence y
	 * strenght no pueda superar el valor 8,
	 */
	private int agility;
	private int defence;
	private int strenght;
	private int life;
	private String name;
	private Dice dice;

	public Javamon() {
		initAtrributes();
	}

	public Javamon(String name) {
		initAtrributes();
		this.name = name;
	}

	public Javamon(int agility, int defence, int strenght, int life, String name, Dice dice) {
		super();
		this.agility = agility;
		this.defence = defence;
		this.strenght = strenght;
		this.life = life;
		this.name = name;
		this.dice = dice;
	}

	private void initAtrributes() {
		Nicknames nicknames = new Nicknames();
		this.name = nicknames.generate();
		this.life = 18;
		generarHabilidades();
		this.dice = new Dice();
	}

	private void generarHabilidades() {

		Random rnd = new Random();
		int cant = 0;
		while (cant < this.life) {
			int puntos = rnd.nextInt(2) + 1;
			if (cant + puntos <= this.life) {
				switch (rnd.nextInt(3)) {
				case 0: {
					if (this.agility + puntos <= 8) {
						this.agility = this.agility + puntos;
						cant = cant + puntos;
						break;
					}
				}
				case 1: {
					if (this.defence + puntos <= 8) {
						this.defence = this.defence + puntos;
						cant = cant + puntos;
						break;
					}
				}
				default: {
					if (this.strenght + puntos <= 8) {
						this.strenght = this.strenght + puntos;
						cant = cant + puntos;
					}
					break;
				}
				}
			} else if (this.defence + this.strenght > 14) {
				this.agility = this.life - cant;
				cant = this.life;
			} else if (this.agility + this.strenght > 14) {
				this.defence = this.life - cant;
				cant = this.life;
			} else if (this.agility + this.defence > 14) {
				this.strenght = this.life - cant;
				cant = this.life;
			}
		}
	}

	public String status() {
		return (this.name + "(" + this.life + ")| S:" + this.strenght + "| D:" + this.defence + "| A:" + this.agility);
	}

	public int defend() {
		return ((this.agility + this.defence / 2) + this.dice.roll());
	}

	public int attack() {
		return (this.strenght + this.dice.roll() + this.agility/2);
	}

	public int initiative() {
		return (this.agility + this.dice.roll());
	}

	public int getAgility() {
		return agility;
	}

	public void setAgility(int agility) {
		this.agility = agility;
	}

	public int getDefence() {
		return defence;
	}

	public void setDefence(int defence) {
		this.defence = defence;
	}

	public int getStrenght() {
		return strenght;
	}

	public void setStrenght(int strenght) {
		this.strenght = strenght;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Dice getDice() {
		return dice;
	}

	public void setDice(Dice dice) {
		this.dice = dice;
	}

}
