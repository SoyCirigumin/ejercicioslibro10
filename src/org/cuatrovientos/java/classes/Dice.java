package org.cuatrovientos.java.classes;

import java.util.Random;

public class Dice {
	/*
	 * Ejercicio 5: Crea una clase llamada Dice para simular el comportamiento de un
	 * dado de N caras. Estos seran los metodos de
	 */

	private int sides;
	private boolean allowZero = false;

	public Dice() {
		this.sides = 6;
	}

	public Dice(int sides) {
		this.sides = sides;
	}

	public Dice(int sides, boolean allowZero) {
		this.sides = sides;
		this.allowZero = allowZero;
	}

	public int roll() {
		Random rnd = new Random();
		int cara=rnd.nextInt(this.sides+1);
		if (this.allowZero == false && cara == 0) {
			return cara+1;
		} else {
			return cara;
		}
	}
}
