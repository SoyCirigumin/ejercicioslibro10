package org.cuatrovientos.java.classes;

import java.util.Random;

public class Coin {
	/*
	 * Ejercicio 2: Crea una clase llamada Coin. La clase debe tener
	 * un constructor vacio y un unico metodo llamado flip() cuyo resultado debe ser
	 * aleatoriamente un numero entero, que en funcion de su valor, mostrara "CROSS"
	 * o "PILE", es decir, cara o cruz. Incluye el metodo main y crea 5 instancias
	 * de la clase para probarla. Crea el diagrama de clases UML
	 */
	private final String CRUZ = "PILE";
	private final String CARA = "CROSS";

	public Coin() {

	}

	public void flip() {
		Random rnd = new Random();
		if (rnd.nextInt(10) % 2 == 0) {
			System.out.println(CARA);
		} else {
			System.out.println(CRUZ);
		}
	}

}
