package org.cuatrovientos.java.classes;

import java.util.Random;

public class Passwords {
	/*
	 * Ejercicio 8:
	 */

	public int lenght;

	public Passwords() {
		this.lenght = 6;
	}

	public Passwords(int lenght) {
		this.lenght = lenght;
	}

	public String generate() {
		String[] vocales = { "a", "e", "i", "o", "u" };
		String[] consonantes = { "q", "w", "r", "t", "y", "p", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c",
				"v", "b", "n", "m" };
		String[] numeros = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		String[] especiales = { "!", "@", "·", "#", "$", "%", "&", "/", "(", ")", "?", "¿", "*", "+", "ç", "´", "-",
				"_", ";", ".", ",", ":", "=" };
		Random rnd = new Random();
		String pass = "";
		for (int i = 0; i < this.lenght; i++) {
			switch (rnd.nextInt(4)) {
			case 0: {
				pass = pass + vocales[rnd.nextInt(5)];
				break;
			}
			case 1: {
				pass = pass + consonantes[rnd.nextInt(21)];
				break;
			}
			case 2: {
				pass = pass + numeros[rnd.nextInt(10)];
				break;
			}
			default:
				pass = pass + especiales[rnd.nextInt(23)];
				break;
			}
		}
		return pass;
	}

	public String generate(int qty) {
		String pass = "";
		for (int i = 0; i < qty; i++) {
			pass = pass + generate() + "\n";
		}
		return pass;
	}
}
