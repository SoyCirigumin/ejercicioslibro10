package org.cuatrovientos.java.classes;

public class Conversor {
	/*
	 * Ejercicio 3: Crea una clase llamada Conversor que tenga varios metodos para convertir
	 * monedas. Incluye el metodo main y crea una instancias de la clase para
	 * probarla. Crea el diagrama de clases UML.
	 * Para definir cada uno de los cambios podria utilizar una constante.
	 * Estos son los metodos que se piden:
	 * 		public double pesetas2Euros(double amount)
	 * 		public double euros2Pesetas(double amount)
	 * 		public double euros2PDollars(double amount)
	 * 		public double dollars2Euros(double amount)
	 * 		public double euros2Pounds(double amount)
	 * 		public double pounds2Euros(double amount)
	 */
	private static final double CHANGE_PESETAS_EUROS = 166.386d;
	private static final double CHANGE_DOLLARS_EUROS = 0.9d;
	private static final double CHANGE_POUNDS_EUROS = 0.8d;

	public double pesetas2Euros(double amount) {
		return (amount / CHANGE_PESETAS_EUROS);
	}

	public double euros2Pesetas(double amount) {
		return (amount * CHANGE_PESETAS_EUROS);
	}

	public double euros2PDollars(double amount) {
		return (amount / CHANGE_DOLLARS_EUROS);
	}

	public double dollars2Euros(double amount) {
		return (amount * CHANGE_DOLLARS_EUROS);
	}

	public double euros2Pounds(double amount) {
		return (amount / CHANGE_POUNDS_EUROS);
	}

	public double pounds2Euros(double amount) {
		return (amount * CHANGE_POUNDS_EUROS);
	}
}
