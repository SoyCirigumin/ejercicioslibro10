package org.cuatrovientos.java.classes;

public class Square {
	/*
	 * Ejercicio 6:
	 */
	private char character;

	public Square() {
		this.character = '#';
	}

	public Square(char character) {
		this.character = character;
	}

	public void setCharacter(char character) {
		this.character = character;
	}

	private String generate(int size) {
		return ejer6Recur(size, size, size, "");
	}

	private String ejer6Recur(int x, int y, int veces, String cadena) {
		if (x == 0) {
			return cadena;
		} else {
			if (y == 0) {
				cadena = cadena + "\n";
				return ejer6Recur(x - 1, veces, veces, cadena);
			} else {
				return ejer6Recur(x, y - 1, veces, cadena + this.character);
			}

		}
	}

	public void show(int sizes) {
		System.out.println(generate(sizes));
	}
}
