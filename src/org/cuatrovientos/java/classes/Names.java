package org.cuatrovientos.java.classes;

import java.util.Random;

public class Names {
	/*
	 * Ejercicio 7:
	 */
	public int lenght;

	public Names() {
		this.lenght = 6;

	}

	public Names(int lenght) {
		this.lenght = lenght;
	}

	public String generate() {
		String[] vocales = { "a", "e", "i", "o", "u" };
		String[] consonantes = { "q", "w", "r", "t", "y", "p", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c",
				"v", "b", "n", "m" };
		String nombre = "";
		Random rnd = new Random();
		if (rnd.nextInt(2) == 0) {
			int cont = 0;
			while (cont < this.lenght - 1) {
				nombre = nombre + vocales[rnd.nextInt(5)];
				nombre = nombre + consonantes[rnd.nextInt(21)];
				cont = cont + 2;
			}
			// para meter la ultima letra cuando el numero en lenght no es par
			if (this.lenght % 2 != 0) {
				nombre = nombre + vocales[rnd.nextInt(5)];
			}

		} else {
			for (int i = 0; i < this.lenght - 1; i = i + 2) {
				nombre = nombre + consonantes[rnd.nextInt(21)];
				nombre = nombre + vocales[rnd.nextInt(5)];
			}
			// para meter la ultima letra cuando el numero en lenght no es par
			if (this.lenght % 2 != 0) {
				nombre = nombre + consonantes[rnd.nextInt(21)];
			}
		}
		return nombre;
	}
}
