package org.cuatrovientos.java.classes;

import java.util.Random;

public class Array {
	/*
	 * Ejercicio 4: Crea una clase llamada Array que sirva para crear un array de
	 * enteros. La clase debe contener un atributo para contener el array de 10
	 * elementos y los siguientes metodos: 
	 * public Array() : Constructor. Se encarga
	 * de invocar (llamar) al siguiente metodo: init.
	 * private void init() : Inicia el array con numeros aleatorios.
	 * Se le llama desde el constructor.
	 * public void increment() : Incrementa cada elemento del array 
	 * en 1.
	 * public void decrement() : Decrementacada elemento en 1.
	 * public int countEvent() : Devuelve el numero de elementos pares
	 * en el array. 
	 * Incluye el metodo main y crea una instancias del array, muestre 
	 * el total de numeros pares, decremente y muestre el total de numeros
	 * pares. Deberia volver al valor inicial.
	 		Random value inserted in 0 : 23
			Random value inserted in 1 : 11
			Random value inserted in 2 : 20
			Random value inserted in 3 : 24
			Random value inserted in 4 : 25
			Random value inserted in 5 : 29
			Random value inserted in 6 : 17
			Random value inserted in 7 : 13
			Random value inserted in 8 : 12
			Random value inserted in 9 : 14
			Total even numbers: 4
			Total even numbers after increment: 6
			Total even numbers after decrement: 4
	* Crea el diagrama UML.
	 */
	
	private int [] numeros; 
	
	public Array() {
		this.numeros = new int [10];
		init();
	}
	
	private void init() {
		Random rnd = new Random();
		for (int i = 0; i < numeros.length; i++) {
			this.numeros[i] = rnd.nextInt(30);
			System.out.println("Random value inserted in " + i + " : " + numeros[i]);
		}
	}
	
	public void increment() {
		for (int i = 0; i < numeros.length; i++) {
			this.numeros[i] = this.numeros[i] + 1;
		}
	}
	
	public void decrement() {
		for (int i = 0; i < numeros.length; i++) {
			this.numeros[i] = this.numeros[i] - 1;
		}
	}
	
	public int countEvent() {
		int contador = 0;
		for (int i = 0; i < numeros.length; i++) {
			if(this.numeros[i] % 2 == 0) {
				contador++;
			}
		}
		return contador;
	}
}
